# Middle Tennessee DSA Design
Unofficial design elements for Middle Tennessee DSA and DSA national.

## Notes
* Before publishing any materials, please ensure that they comply with DSA National's style guide (DSA Style Guide.pdf). 
* Use vector formats whenever possible (SVG/EPS), they can be expanded to any size.
* When printing, use at least 300 DPI.
* Do not expand raster images (jpg, png, etc.) above their original size or 72 DPI.

Font: Manifold DSA
Red: `#EC1F27`
Black: `#231F20`

### Software
If you don't already have an Adobe CC license, there are free open-source alternatives. 
* **Photoshop:** [GNU Image Manipulation Program (GIMP)](https://gimp.org) or [Paint .NET](https://getpaint.net)
* **Illustrator:** [Inkscape](https://inkscape.org)
